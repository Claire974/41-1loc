# Projet 1'Loc

## Documentation

La documentation est disponible [ici](docs/index.md).

## A propos

Le projet 1'Loc est un projet fictif, dont le but est de comprendre le workflow d'un projet allant de la conception au déploiement en passant par les test. Le tout avec l'utilisation du framework Symfony. Ce projet est réalisé dans le cadre de la formation CDA de l'école M2I Grenoble.