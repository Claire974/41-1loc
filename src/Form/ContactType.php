<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'contact.name.placeholder',
                    ],
                    'constraints' => [
                        new NotBlank(),
                    ],
                ],
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => false,
                    'attr' => [
                        'placeholder' => 'contact.email.placeholder',
                    ],
                    'help' => 'contact.email.help',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'contact.email.blank',
                        ]), new Email([
                            'message' => 'contact.email.invalid',
                        ]),
                    ],
                ],
            )
            ->add(
                'subject',
                ChoiceType::class,
                [
                    'choices' => [
                            'contact.subject.choices.one' => 1,
                            'contact.subject.choices.two' => 2,
                            'contact.subject.choices.three' => 3,
                        ],
                        'label' => 'contact.subject.label',
                        'placeholder' => 'contact.subject.placeholder',
                        'constraints' => [
                            new NotBlank([
                                'message' => 'contact.subject.blank',
                            ]),
                        ],
                ]
            )
            ->add(
                'message',
                TextareaType::class,
                [
                    'label' => 'contact.message.label',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'contact.msg.blank',
                        ]),
                    ],
                ]
            )
            ->add(
                'gdpr',
                CheckboxType::class,
                [
                    'label' => 'contact.gdpr.label',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'contact.gdpr.blank',
                        ]),
                    ],
                ]
            )
        ;
        $builder->get('subject')->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $this->addDateField($event);
        });
        $builder->get('subject')->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
            $this->addDateField($event);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }

    private function addDateField(FormEvent $event): void
    {
        $form = $event->getForm()->getParent();
        $data = $event->getData();

        if ($form instanceof FormInterface) {
            if ($data == 2) {
                $form->add('date', DateType::class, [
                    'label' => 'contact.date',
                    'input' => 'string',
                    'widget' => 'single_text',
                    'constraints' => [
                        new NotBlank([
                            'message' => 'contact.date.blank',
                        ]),
                    ],
                ]);
            }
        }
    }
}
