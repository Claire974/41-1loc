<?php

namespace App\Form\Admin;

use App\Entity\Reservation;
use App\Entity\Room;
use App\Entity\User;
use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('dateStart', DateTimeType::class, [
                'label' => 'Début',
                'html5' => false,
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy HH:mm',
                'attr' => [
                    'class' => 'js-datetimepicker',
                    'data-min' => date('d-m-Y H', strtotime('+2 hours')).':00',
                ],
                'empty_data' => (new DateTime())->format('d-m-Y H:i'),
            ])
            ->add('dateEnd', DateTimeType::class, [
                'label' => 'Fin',
                'html5' => false,
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy HH:mm',
                'attr' => [
                    'class' => 'js-datetimepicker',
                    'data-min' => date('d-m-Y H', strtotime('+3 hours')).':00',
                ],
                'empty_data' => (new DateTime())->format('d-m-Y H:i'),
            ])
            ->add('room', EntityType::class, [
                'label' => 'Salle',
                'class' => Room::class,
                'choice_label' => 'name',
                'placeholder' => 'Choisir une salle',
            ])
            ->add('user', EntityType::class, [
                'label' => 'Client',
                'class' => User::class,
                'choice_label' => 'fullName',
                'placeholder' => 'Choisir un client',
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'Commentaire (optionel)',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
