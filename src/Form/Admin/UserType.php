<?php

namespace App\Form\Admin;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Adresse mail',
                'empty_data' => '',
            ])
            ->add('firstName', TextType::class, [
                'label' => 'prénom',
                'empty_data' => '',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'nom',
                'empty_data' => '',
            ])
            ->add('companyName', TextType::class, [
                'label' => 'Nom entreprise',
                'required' => false,
            ])
            ->add('siret', TextType::class, [
                'label' => 'Siret',
                'required' => false,
            ])
            ->add('address', TextType::class, [
                'label' => 'adresse',
                'empty_data' => '',
            ])
            ->add('zipCode', TextType::class, [
                'label' => 'code postal',
                'empty_data' => '',
            ])
            ->add('city', TextType::class, [
                'label' => 'ville',
                'empty_data' => '',
            ])
            ->add('phone', TextType::class, [
                'label' => 'numéro de téléphone',
                'empty_data' => '',
            ])
            ->add('comment', TextType::class, [
                'label' => 'Commentaire',
                'required' => false,
            ])
            ->add('roles', ChoiceType::class, [
                'choices' => [
                    'Administrateur' => 'ROLE_ADMIN',
                    'Utilisateur' => 'ROLE_USER',
                ],
                'required' => false,
                'expanded' => true,
                'multiple' => true,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
