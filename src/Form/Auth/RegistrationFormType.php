<?php

namespace App\Form\Auth;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Adresse mail *',
                'empty_data' => '',
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Votre prénom *',
                'empty_data' => '',
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Votre nom *',
                'empty_data' => '',
            ])
            ->add('companyName', TextType::class, [
                'label' => 'Nom entreprise',
                'required' => false,
            ])
            ->add('siret', TextType::class, [
                'label' => 'Siret',
                'required' => false,
            ])
            ->add('address', TextType::class, [
                'label' => 'Votre adresse *',
                'empty_data' => '',
            ])
            ->add('zipCode', TextType::class, [
                'label' => 'Votre code postal *',
                'empty_data' => '',
            ])
            ->add('city', TextType::class, [
                'label' => 'Votre ville *',
                'empty_data' => '',
            ])
            ->add('phone', TextType::class, [
                'label' => 'Votre numéro de téléphone *',
                'empty_data' => '',
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'label' => 'En cochant cette case, je donne mon consentement à l\'enregistrement de mes données sur le site 1\'Loc pour la création de mon compte.',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter notre politique de confidentialtié',
                    ]),
                ],
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'first_options' => [
                    'label' => 'Votre mot de passe *',
                    'help' => '8 caractères minimum, 1 majuscule, 1miniscule, 1 chiffre',
                ],
                'second_options' => [
                    'label' => 'Confirmation du mot de passe',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
