<?php

namespace App\Twig;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private RequestStack $requestStack;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('active', [$this, 'active']),
        ];
    }

    public function active(string $value): ?string
    {
        $request = $this->requestStack->getCurrentRequest();
        if ($request instanceof Request) {
            if (str_contains($request->attributes->get('_route'), $value)) {
                return 'active';
            } else {
                return null;
            }
        } else {
            return null;
        }
        // return str_contains($this->requestStack->getCurrentRequest(), $value)?'active':null;
    }

    public function getRequestStack(): RequestStack
    {
        return $this->requestStack;
    }

    public function setRequestStack(RequestStack $requestStack): self
    {
        $this->requestStack = $requestStack;

        return $this;
    }
}
