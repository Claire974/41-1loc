<?php

namespace App\Service;

use Dompdf\Dompdf;
use Symfony\Component\Filesystem\Filesystem;

class PdfHelper
{
    public function generatePdf(string $name, string $destination, string $content, ?string $orientation = 'portrait'): void
    {
        $pdf = new Dompdf();
        $pdf->loadHtml($content);
        if ($orientation !== 'portrait' && $orientation !== 'landscape') {
            $orientation = 'portrait';
        }
        $pdf->setPaper('A4', $orientation);
        $pdf->render();

        $output = $pdf->output();

        $filesystem = new Filesystem();
        if (!$filesystem->exists($destination)) {
            $filesystem->mkdir($destination);
        }

        if (!file_put_contents($destination.$name, $output)) {
        }
    }
}
