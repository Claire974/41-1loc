<?php

namespace App\Controller;

use App\Repository\ReservationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="api")
     */
    public function index(ReservationRepository $reservationRepository): JsonResponse
    {
        //Attention il faudrait protéger l'api avec une connexion
        return new JsonResponse($reservationRepository->getScheduledReservation());
    }
}
