<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Entity\User;
use App\Form\Profile\EditPasswordType;
use App\Form\Profile\ProfileType;
use App\Repository\ReservationRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ProfileController extends AbstractController
{
    /**
     * @Route("/mon-compte", name="profile_show")
     */
    public function show(): Response
    {
        return $this->render('profile/show.html.twig');
    }

    /**
     * @Route("/mon-compte/modifier", name="profile_edit")
     */
    public function edit(Request $request): Response
    {
        $user = $this->getUser();
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Votre compte a bien été modifié');

            return $this->redirectToRoute('profile_show');
        }

        return $this->renderForm('profile/edit.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/mon-compte/modifier-mot-de-passe", name="profile_edit_password")
     */
    public function editPassword(Request $request, UserPasswordHasherInterface $passwordHasher): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(EditPasswordType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordHasher->hashPassword($user, $form->get('password')->getData());
            $user->setPassword($password);
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'Votre mot de passe a bien été modifié');

            return $this->redirectToRoute('profile_show');
        }

        return $this->renderForm('profile/edit_password.html.twig', [
            'form' => $form,
        ]);
    }

    /**
     * @Route("/mon-compte/mes-reservations", name="profile_reservation")
     */
    public function showReservation(ReservationRepository $reservationRepository, PaginatorInterface $paginator, Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $reservations = $paginator->paginate(
            $reservationRepository->showUserReservation($user),
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('profile/show_reservation.html.twig', [
            'reservations' => $reservations,
        ]);
    }

    /**
     * @Route("/mon-compte/mes-reservations/{id}", name="profile_reservation_cancel", methods={"POST"})
     */
    public function cancelReservation(Request $request, Reservation $reservation): Response
    {
        $this->denyAccessUnlessGranted('RESERVATION_CANCEL', $reservation);

        if ($this->isCsrfTokenValid('cancel'.$reservation->getId(), (string) $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $reservation->setStatus(-1);
            $entityManager->flush();
            $this->addFlash('success', 'Votre réservation a bien été annulée');
        }

        return $this->redirectToRoute('profile_reservation', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/mon-compte/supprimer/{id}", name="profile_delete", methods={"POST"})
     */
    public function delete(Request $request, TokenStorageInterface $tokenStorage, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), (string) $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();

            $tokenStorage->setToken();
            $request->getSession()->invalidate();

            $session = new Session();
            $session->invalidate();

            return $this->redirectToRoute('profile_account_deleted', [], Response::HTTP_SEE_OTHER);
        }
        $this->addFlash('danger', 'Erreur lors de la suppression du compte. Veuillez réessayer plus tard');

        return $this->redirectToRoute('profile_show', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/compte-supprime", name="profile_account_deleted")
     */
    public function accountDeleted(): Response
    {
        return $this->render('profile/account_deleted.html.twig');
    }
}
