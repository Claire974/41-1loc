<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Entity\User;
use App\Event\CreateReservationEvent;
use App\Form\ContactType;
use App\Form\ReservationType;
use App\Repository\CategoryRepository;
use App\Repository\ReservationRepository;
use App\Repository\RoomRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class SiteController extends AbstractController
{
    /**
     * @Route("/", name="site_index")
     */
    public function index(): Response
    {
        return $this->render('site/index.html.twig');
    }

    /**
     * @Route("/fonctionnement", name="site_about")
     */
    public function about(): Response
    {
        return $this->render('site/about.html.twig');
    }

    /**
     * @Route("/mentions-legales", name="site_legals")
     */
    public function legals(): Response
    {
        return $this->render('site/legals.html.twig');
    }

    /**
     * @Route("/contact", name="site_contact")
     * @Route("/contact/{action}", name="site_contact")
     */
    public function contact(Request $request, TranslatorInterface $translator, MailerInterface $mailer, string $action = null): Response
    {
        if ($action == 'reservation') {
            $action = 2;
        } else {
            $action = 0;
        }

        $form = $this->createForm(ContactType::class, ['subject' => $action]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $name = $form->get('name')->getData();
            $email = $form->get('email')->getData();
            $message = $form->get('message')->getData();
            $subject = $form->get('subject');
            $choices = $subject->getConfig()->getOption('choices');
            $object = array_search($subject->getData(), $choices);
            $objectTranslated = $translator->trans($object);

            $date = $form->has('date') ? $form->get('date')->getData() : null;

            $email_templated = (new TemplatedEmail())
                ->from(new Address($email, $name))
                ->to('paul@auchon.local')
                ->subject($objectTranslated)
                ->textTemplate('mail/contact.txt.twig')
                ->context([
                    'name' => $name,
                    'mail' => $email,
                    'message' => $message,
                    'date' => $date,
                ])
            ;

            try {
                $mailer->send($email_templated);
                $this->addFlash('success', 'Votre message a bien été envoyé');
            } catch (TransportExceptionInterface $e) {
                $this->addFlash('danger', 'Une erreur est survenue, veuillez réessayer plus tard');
            }

            return $this->redirectToRoute('site_contact');
        }

        return $this->render('site/contact.html.twig', [
            'contact_form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/nos-salles", name="site_show_all_rooms")
     */
    public function showAllRooms(RoomRepository $roomRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $rooms = $paginator->paginate(
            $roomRepository->findAllActiveRooms(),
            $request->request->getInt('page', 1),
            12
        );

        return $this->render('site/show_all_rooms.html.twig', [
            'rooms' => $rooms,
        ]);
    }

    /**
     * @Route("/nos-salles/{slug}", name="site_show_room")
     */
    public function showRoom(RoomRepository $roomRepository, string $slug): Response
    {
        $room = $roomRepository->findRoom($slug);

        if ($room == null) {
            throw $this->createNotFoundException();
        }

        return $this->render('site/show_room.html.twig', [
            'room' => $room,
        ]);
    }

    /**
     * @Route("/nos-salles/categorie/{slug}", name="site_show_category")
     */
    public function showCategory(CategoryRepository $catRepo, PaginatorInterface $paginator, Request $request, string $slug): Response
    {
        $cat = $catRepo->findCategory($slug);

        if ($cat === null) {
            throw $this->createNotFoundException();
        }

        $rooms = $paginator->paginate(
            $cat->getRooms(),
            $request->request->getInt('page', 1),
            12
        );

        return $this->render('site/show_category.html.twig', [
            'category' => $cat,
            'rooms' => $rooms,
        ]);
    }

    /**
     * @Route("/nos-salles/{slug}/reserver", name="site_room_reservation")
     * @IsGranted("ROLE_USER")
     */
    public function reservation(
        RoomRepository $roomRepository,
        ReservationRepository $reservRepo,
        Request $request,
        EventDispatcherInterface $dispatcher,
        string $slug
    ): Response {
        /** @var User $user */
        $user = $this->getUser();
        $room = $roomRepository->findRoom($slug);

        if ($room == null) {
            throw $this->createNotFoundException();
        }

        $reservation = (new Reservation())
            ->setRoom($room)
            ->setUser($user);
        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $available = $reservRepo->checkAvailable(
                $form->get('dateStart')->getData(),
                $form->get('dateEnd')->getData(),
                (int) $room->getId()
            );
            if (!$available) {
                $form->addError(new FormError('La salle n\'est pas disponible, veuillez choisir un autre créneau'));

                return $this->renderForm('site/reservation.html.twig', [
                    'reservation' => $reservation,
                    'room' => $room,
                    'form' => $form,
                ]);
            }

            $dispatcher->dispatch(new CreateReservationEvent($reservation));
            $this->addFlash('success', 'Votre réservation est confirmée.');

            return $this->redirectToRoute('profile_reservation');
        }

        return $this->renderForm('site/reservation.html.twig', [
            'reservation' => $reservation,
            'room' => $room,
            'form' => $form,
        ]);
    }
}
