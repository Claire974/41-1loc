<?php

namespace App\Controller\Admin;

use App\Entity\Room;
use App\Form\Admin\RoomType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/salle")
 */
class RoomController extends AbstractController
{
    /**
     * @Route("/", name="admin_room_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request): Response
    {
        $rooms = $paginator->paginate(
            $entityManager->createQuery('SELECT r FROM App:Room r'),
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('admin/room/index.html.twig', [
            'rooms' => $rooms,
        ]);
    }

    /**
     * @Route("/ajouter", name="admin_room_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $room = new Room();
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($room);
            $entityManager->flush();
            $this->addFlash('success', 'La salle a bien été ajoutée');

            return $this->redirectToRoute('admin_room_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/room/new.html.twig', [
            'room' => $room,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="admin_room_show", methods={"GET"})
     */
    public function show(Room $room): Response
    {
        return $this->render('admin/room/show.html.twig', [
            'room' => $room,
        ]);
    }

    /**
     * @Route("/{id}/modifier", name="admin_room_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Room $room): Response
    {
        $form = $this->createForm(RoomType::class, $room);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success', 'La salle a bien été modifiée');

            return $this->redirectToRoute('admin_room_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/room/edit.html.twig', [
            'room' => $room,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="admin_room_delete", methods={"POST"})
     */
    public function delete(Request $request, Room $room): Response
    {
        if ($this->isCsrfTokenValid('delete'.$room->getId(), (string) $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($room);
            $entityManager->flush();
            $this->addFlash('success', 'La salle a bien été supprimée');
        }

        return $this->redirectToRoute('admin_room_index', [], Response::HTTP_SEE_OTHER);
    }

    /**
     * @Route("/switch/{id}", name="admin_room_switch_active", methods={"PATCH"})
     */
    public function toggleIsActive(Room $room, EntityManagerInterface $em): JsonResponse
    {
        $room->setIsActive(!$room->getIsActive());
        $em->flush();

        return new JsonResponse([
            'value' => $room->getIsActive(),
        ], 200);
    }
}
