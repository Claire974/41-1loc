<?php

namespace App\Controller\Admin;

use App\Entity\Reservation;
use App\Event\CreateReservationEvent;
use App\Form\Admin\ReservationType;
use App\Repository\ReservationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/reservation")
 */
class ReservationController extends AbstractController
{
    /**
     * @Route("/", name="admin_reservation_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request): Response
    {
        $reservations = $paginator->paginate(
            $entityManager->createQuery('SELECT r FROM App:Reservation r'),
            $request->query->getInt('page', 1),
            20
        );

        return $this->render('admin/reservation/index.html.twig', [
            'reservations' => $reservations,
        ]);
    }

    /**
     * @Route("/ajouter", name="admin_reservation_new", methods={"GET","POST"})
     */
    public function new(Request $request, ReservationRepository $reservRepo, EventDispatcherInterface $dispatcher): Response
    {
        $reservation = new Reservation();
        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $room = $form->get('room')->getData();
            $start = $form->get('dateStart')->getData();
            $end = $form->get('dateEnd')->getData();

            $available = $reservRepo->checkAvailable($start, $end, $room->getId());
            if (!$available) {
                $form->addError(new FormError('La salle n\'est pas disponible, veuillez choisir un autre créneau'));

                return $this->renderForm('admin/reservation/new.html.twig', [
                    'reservation' => $reservation,
                    'form' => $form,
                ]);
            }
            $dispatcher->dispatch(new CreateReservationEvent($reservation));

            $this->addFlash('success', 'La réservation a bien été ajoutée');

            return $this->redirectToRoute('admin_reservation_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin/reservation/new.html.twig', [
            'reservation' => $reservation,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="admin_reservation_show", methods={"GET"})
     */
    public function show(Reservation $reservation): Response
    {
        return $this->render('admin/reservation/show.html.twig', [
            'reservation' => $reservation,
        ]);
    }

    /**
     * @Route("/{id}", name="admin_reservation_cancel", methods={"POST"})
     */
    public function cancel(Request $request, Reservation $reservation): Response
    {
        $this->denyAccessUnlessGranted('RESERVATION_CANCEL', $reservation);

        if ($this->isCsrfTokenValid('cancel'.$reservation->getId(), (string) $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $reservation->setStatus(-1);
            $entityManager->flush();
            $this->addFlash('success', 'La réservation a bien été annulée');
        }

        return $this->redirectToRoute('admin_reservation_index', [], Response::HTTP_SEE_OTHER);
    }
}
