<?php

namespace App\Controller\Admin;

use App\Repository\ReservationRepository;
use App\Repository\RoomRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin_index")
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig');
    }

    /**
     * @Route("/admin/statistique", name="admin_stat")
     */
    public function stat(ReservationRepository $reservationRepository, RoomRepository $roomRepository): Response
    {
        $start = date('Y-m-d', strtotime('monday this week')).' 00:00:01';
        $end = date('Y-m-d', strtotime('sunday this week')).' 23:59:59';
        $dailyIncome = $reservationRepository->DailyIncomeForRange($start, $end);

        $incomes = '';
        $days = '';
        $count = count($dailyIncome);

        foreach ($dailyIncome as $key => $value) {
            $incomes .= $value['total'];
            $days .= date('d-m-Y', strtotime($value['day']));
            if ($key !== $count - 1) {
                $incomes .= ',';
                $days .= ',';
            }
        }

        $roomsReserved = count($reservationRepository->roomReservedToday());
        $nbRooms = $roomRepository->numberOfRoomsActive();

        $chart1 = ['incomes' => $incomes, 'days' => $days];
        $chart2 = ['reserved' => $roomsReserved, 'total' => $nbRooms];

        return $this->render('admin/stat.html.twig', [
            'chart1' => $chart1,
            'chart2' => $chart2,
        ]);
    }
}
