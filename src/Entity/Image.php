<?php

namespace App\Entity;

use App\Repository\ImageRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass=ImageRepository::class)
 * @Vich\Uploadable()
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $path = '';

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private string $name = '';

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTimeInterface $updatedAt;

    /**
     * @Vich\UploadableField(mapping="image", fileNameProperty="path")
     * @Assert\Image(
     *     maxSize="2M",
     *     mimeTypes = {"image/jpeg", "image/png"},
     *     maxSizeMessage="Votre fichier doit faire moins de {{ limit }}Mo",
     *     mimeTypesMessage="Le fichier doit être un JPG ou PNG",
     *     notFoundMessage="Le fichier n'a pas été trouvé",
     *     uploadErrorMessage="Erreur lors du téléchargement du fichier"
     * )
     */
    private ?File $imageFile = null;

    /**
     * @ORM\ManyToOne(targetEntity=Room::class, inversedBy="images")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Room $room;

    public function __construct()
    {
        $this->updatedAt = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path ?: '';

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageFile(?File $imageFile = null): self
    {
        $this->imageFile = $imageFile;

        if ($imageFile instanceof UploadedFile) {
            $this->updatedAt = new DateTime();
        }

        return $this;
    }
}
