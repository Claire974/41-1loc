<?php

namespace App\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user): void
    {
    }

    public function checkPostAuth(UserInterface $user): void
    {
        if (empty($user->getRoles())) {
            throw new CustomUserMessageAccountStatusException('Votre compte est banni');
        }

        if ($user instanceof User && !$user->isVerified()) {
            throw new CustomUserMessageAccountStatusException('Vous devez valider votre adresse mail avant de pouvoir vous connecter');
        }
    }
}
