<?php

namespace App\Security\Voter;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\Security;

class ManageUserVoter extends Voter
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports(string $attribute, $subject): bool
    {
        // replace with your own logic
        // https://symfony.com/doc/current/security/voters.html
        return in_array($attribute, ['USER_EDIT', 'USER_DELETE']) && $subject instanceof User;
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();
        if (!$user instanceof User && !$this->security->isGranted('ROLE_ADMIN')) {
            return false;
        }

        /** @var User $target */
        $target = $subject;

        // ... (check conditions and return true to grant permission) ...
        switch ($attribute) {
            case 'USER_EDIT':
            case 'USER_DELETE':
                /** @var User $user */
                return $this->canManage($target, $user);
        }

        return false;
    }

    private function canManage(USer $target, User $user): bool
    {
        if ($target->getId() === $user->getId()) {
            return false;
        }

        return true;
    }
}
