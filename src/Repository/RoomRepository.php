<?php

namespace App\Repository;

use App\Entity\Room;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Room|null find($id, $lockMode = null, $lockVersion = null)
 * @method Room|null findOneBy(array $criteria, array $orderBy = null)
 * @method Room[]    findAll()
 * @method Room[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RoomRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Room::class);
    }

    /**
     * @return Room[] Returns an array of Room objects
     */
    public function findAllActiveRooms(): array
    {
        return $this->createQueryBuilder('r')
            ->addSelect('c', 'i')
            ->andWhere('r.isActive = 1')
            ->leftJoin('r.category', 'c')
            ->leftJoin('r.images', 'i')
            ->orderBy('r.id', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function findRoom(string $slug): ?Room
    {
        return $this->createQueryBuilder('r')
            ->addSelect('c', 'i')
            ->andWhere('r.isActive = 1')
            ->leftJoin('r.category', 'c')
            ->leftJoin('r.images', 'i')
            ->andWhere('r.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function numberOfRoomsActive(): int
    {
        return $this->createQueryBuilder('r')
            ->select('count(r.id)')
            ->andWhere('r.isActive = 1')
            ->getQuery()
            ->getSingleScalarResult()
        ;
    }
}
