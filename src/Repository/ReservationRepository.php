<?php

namespace App\Repository;

use App\Entity\Reservation;
use DateTime;
use DateTimeInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @method Reservation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Reservation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Reservation[]    findAll()
 * @method Reservation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReservationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Reservation::class);
    }

    public function checkAvailable(DateTimeInterface $start, DateTimeInterface $end, int $room): bool
    {
        $result = $this->createQueryBuilder('r')
            ->andWhere('r.status = 0')
            ->andWhere('r.room = :room')
            ->andWhere('
                :start between r.dateStart and r.dateEnd
                or :end between r.dateStart and r.dateEnd
                or r.dateStart between :start and :end
                or r.dateEnd between :start and :end
                ')
            ->setParameters([
                'room' => $room,
                'start' => $start,
                'end' => $end,
            ])
            ->getQuery()
            ->getResult()
        ;

        return !$result;
    }

    /**
     * @return Reservation[]
     */
    public function showUserReservation(UserInterface $user): array
    {
        return $this->createQueryBuilder('r')
            ->addSelect('ro')
            ->leftJoin('r.room', 'ro')
            ->andWhere('r.user = :user')
            ->setParameter('user', $user)
            ->orderBy('r.dateEnd', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    public function DailyIncomeForRange(string $start, string $end): array
    {
        return $this->createQueryBuilder('r')
            ->select('sum(r.total) as total, substring(r.dateStart, 1, 10) as day')
            ->andWhere('r.status = 0')
            ->andWhere('r.dateStart between :start and :end')
            ->setParameters([
                'start' => $start,
                'end' => $end,
            ])
            ->groupBy('day')
            ->getQuery()
            ->getResult()
        ;
    }

    public function roomReservedToday(): array
    {
        return $this->createQueryBuilder('r')
            ->select('count(r.id)')
            ->andWhere('r.status = 0')
            ->andWhere('r.dateStart between :start and :end')
            ->andWhere('r.dateEnd between :start and :end')
            ->setParameters([
                'start' => (new DateTime())->format('Y-m-d').' 00:00:01',
                'end' => (new DateTime())->format('Y-m-d').' 23:59:59',
            ])
            ->groupBy('r.room')
            ->getQuery()
            ->getResult()
        ;
    }

    public function getScheduledReservation(): array
    {
        return $this->createQueryBuilder('r')
            ->select('r.dateStart', 'r.dateEnd', 'r.code', 'ro.id')
            ->leftJoin('r.room', 'ro')
            ->andWhere('r.status = 0')
            ->andWhere('r.dateEnd > :now')
            ->setParameter('now', new DateTime())
            ->getQuery()
            ->getResult()
        ;
    }

    /*
    public function findOneBySomeField($value): ?Reservation
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
