<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Room;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private UserPasswordHasherInterface $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        $faker = Faker\Factory::create('fr_FR');

        $admin = (new User())
            ->setEmail('admin@admin.local')
            ->setFirstName('Martine')
            ->setLastName('Aston')
            ->setPhone('0612345678')
            ->setAddress('22 rue bidon')
            ->setZipCode('38100')
            ->setCity('Grenoble')
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN'])
            ->setIsVerified(true)
        ;

        $admin->setPassword($this->passwordHasher->hashPassword($admin, 'PasswordAdmin'));
        $manager->persist($admin);

        for ($c = 0; $c < 10; $c++) {
            $category = (new Category())->setName($faker->company());
            $manager->persist($category);
            $this->addReference('category'.$c, $category);
        }

        for ($i = 0; $i < 50; $i++) {
            /** @var Category $category */
            $category = $this->getReference('category'.mt_rand(0, 9));
            $room = (new Room())
                ->setName($faker->company())
                ->setSeat($faker->numberBetween(1, 30))
                ->setSurface($faker->randomFloat(2, 5, 30))
                ->setPrice($faker->randomFloat(2, 10, 70))
                ->setDescription($faker->text())
                ->setIsActive($faker->boolean(80))
                ->setCategory($category)
                ;
            $manager->persist($room);
        }

        $manager->flush();
    }
}
