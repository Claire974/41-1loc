import {ajax} from './utils/ajax'
import {htmlToElement} from './utils/htmlElement'

document.querySelector('#contact_subject').addEventListener('change', function() {
    let field = this
    let form = field.closest('form')
    let data = {}
    let target = '#' + field.getAttribute('id').replace('subject', 'date')
    data[field.getAttribute('name')] = field.value
    ajax(form.action, 'POST', data).then(result => {
        //result =  toute la page HTML (avec le champ date) au format chaine de caratère
        //On recupère la position du debut du formulaire
        const formStart = result.indexOf('<form name="contact" method="post">')
        //On recupère la position de la fin du formulaire
        const formEnd = result.indexOf('</form>')+ 7  // +7 pour garder la fermeture de la balise form
        //On calule le nombre de caractère du début à la fin du formulaire pour l'extraction
        const length = formEnd - formStart
        //newForm = tout le formulaire en chaine de caractère
        let newForm = result.substr(formStart, length)
        //On utilise la fonction htmlToElement pour transformer notre chaine de caractère (le form) en élement html
        let newFormElement = htmlToElement(newForm)
        //On récupère l'input date s'il existe sinon null
        let input = newFormElement.querySelector(target)
        if (input) {
            const inputGroup = input.parentElement
            inputGroup.querySelector('.invalid-feedback').remove()
            inputGroup.querySelector(target).classList.remove('is-invalid')
            //Après le parent du champ objet on ajoute notre inputGroup
            // https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentElement
            field.parentNode.insertAdjacentElement('afterend', inputGroup)
        } else {
            let object = form.querySelector(target)
            if (object) object.parentElement.remove()
        }
    })
})