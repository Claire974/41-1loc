import {
    Chart,
    ArcElement,
    BarElement,
    BarController,
    PieController,
    CategoryScale,
    LinearScale,
    Legend,
    Title,
    Tooltip,
} from 'chart.js';

Chart.register(
    ArcElement,
    BarElement,
    BarController,
    PieController,
    CategoryScale,
    LinearScale,
    Legend,
    Title,
    Tooltip,
);

const chart1 = document.querySelector('#chart1');
const days = chart1.dataset.days.split(',');
const incomes = chart1.dataset.incomes.split(',').map(x => +x);

new Chart(chart1,{
    type: 'bar',
    data: {
        labels: days,
        datasets: [{
            label: 'Revenu par jour',
            data: incomes,
            backgroundColor: ['rgba(255, 99, 132, 0.2)','rgba(255, 159, 64, 0.2)','rgba(255, 205, 86, 0.2)','rgba(75, 192, 192, 0.2)','rgba(54, 162, 235, 0.2)','rgba(153, 102, 255, 0.2)','rgba(201, 203, 207, 0.2)'],
            borderColor: ['rgb(255, 99, 132)','rgb(255, 159, 64)','rgb(255, 205, 86)','rgb(75, 192, 192)','rgb(54, 162, 235)','rgb(153, 102, 255)','rgb(201, 203, 207)'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    },
});

const chart2 = document.querySelector('#chart2');
const reserved = chart2.dataset.reserved;
const total = chart2.dataset.total;
const data = [reserved, total - reserved];

new Chart(chart2,{
    type: 'pie',
    data: {
        labels: [ 'Occupé', 'Libre' ],
        datasets: [{
            label: 'Taux d\'occupation',
            data: data,
            backgroundColor: ['rgba(255, 99, 132, 0.2)','rgba(255, 159, 64, 0.2)'],
            borderColor: ['rgb(255, 99, 132)','rgb(255, 159, 64)'],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            y: {
                beginAtZero: true
            }
        }
    },
});