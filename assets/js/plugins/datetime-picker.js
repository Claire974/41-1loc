require("flatpickr/dist/flatpickr.min.css");

import flatpickr from "flatpickr";
import { French } from "flatpickr/dist/l10n/fr"
document.querySelectorAll('.js-datetimepicker').forEach(el => {
    let min = el.dataset.min;
    flatpickr(el, {
        enableTime: true,
        dateFormat: 'd-m-Y H:i',
        minDate: min,
        time_24hr: true,
        minuteIncrement: 0,
        locale: French
    });
});
document.querySelectorAll('.flatpickr-calendar input').forEach(el => {
    el.setAttribute('disabled', 'disabled');
});