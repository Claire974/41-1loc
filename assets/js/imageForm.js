import {htmlToElement} from './utils/htmlElement'

const collectionImage = document.querySelector('#images')
const addBtn = document.querySelector('#add-new')
let length = collectionImage.childElementCount
collectionImage.dataset.index  = length.toString()

addBtn.addEventListener('click', () => {
    const prototype = collectionImage.dataset.prototype
    let template = prototype.replace(/__name__/g, length)
    let inputs = htmlToElement(template)
    collectionImage.dataset.index = (length++).toString()
    collectionImage.appendChild(inputs)
})

document.addEventListener('click', function(e) {
    if (e.target.matches('.js-delete')) {
        e.target.closest('.js-line').remove()
    }
})
