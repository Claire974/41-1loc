import {ajax} from '../utils/ajax'

export default class SwitchActive {
    constructor() {
        document.querySelectorAll('.js-switch').forEach(el => {
            el.addEventListener('click', e => {
                e.preventDefault();
                const btn = el;
                const url = el.href;
                console.log(url);
                ajax(url, 'PATCH', null, 'json').then(result => {
                    let text = result.value ? 'Actif' : 'Inactif'
                    let className = result.value ? 'success' : 'danger'
                    btn.innerHTML = `<span class="badge bg-${className}">${text}</span>`
                }).catch( error => {
                    console.error(error)
                })
            })
        })
    }
}