export function ajax(url, method, data, type) {
    let params = null;

    if (data) {
        //Encodage des données pour l'envoi
        params = typeof data === 'string' ? data : Object.keys(data).map(
            function (k) { return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
        ).join('&');
    }

    return new Promise(function(resolve, reject) {
        let request = new XMLHttpRequest();
        request.open(method, url, true);
        request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        data ? request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded') : null ;
        type ? request.responseType = type : null;
        request.onreadystatechange = function () {
            if (request.readyState === 4) {
                if (request.status === 200) {
                    resolve(request.response);
                } else {
                    reject(request)
                }
            }
        }
        request.send(params);
    })
}