# UML

[Retour au sommaire](index.md)

* [Cas d'utilisation global](#cas-dutilisation)
    * [Gestion des salles](#gestion-des-salles)
* [Diagramme de classe](#diagramme-de-classe)
* [Sitemap](#sitemap)

## Cas d'utilisation
[![Cas d'utilisation](uml/img/usecase.png)](uml/diagramme-cas-utilisation.puml)

### Gestion des salles
[![Gestion des salles](uml/img/gestion-salle.png)](uml/gestion_salle.puml)

## Diagramme de classe
[![Gestion de classe](uml/img/Classe.png)](uml/diag-classe.puml)

## Sitemap

### Visiteur
[![Sitemap visiteur](uml/img/visiteur.png)](uml/sitemap/visiteur.puml)

### Client
[![Sitemap client](uml/img/client.png)](uml/sitemap/client.puml)

### Employe
[![Sitemap employe](uml/img/employe.png)](uml/sitemap/employe.puml)