# Environnements

[Retour au sommaire](index.md)

## Pré-requis
* PHP >= 7.4
* Extensions PHP :
    * ctype
    * iconv
    * json
    * SQLite
* Composer
* MySQL >= 5.7

Il existe plusieurs env différents:
* `prod`: environnement de production (site en ligne)
* `dev` : environnement de développement
* `test` : environnement pour les tests

Pour chaque environnement, il sera nécessaire de créer un fichier contenant les variables d'environnement.

## Exemples

Pour les tests : `.env.dev` ou `.env.dev.local`
```dotenv
DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
```