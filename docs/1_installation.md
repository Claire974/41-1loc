# Installation

[Retour au sommaire](index.md)

## Récupérer les sources du projet
```
git clone git@gitlab.com:Claire974/41-1loc.git
```

## Pré-requis
* PHP >= 7.4
* Extensions PHP :
    * ctype
    * iconv
    * json
    * SQLite
* Composer
* MySQL >= 5.7
* NodeJS >= 14.15
* npm >= 6.14
* yarn >= 1.22

## Installer les dépendances
Se positionner dans le dossier du projet :
```
cd 41-1loc
```

Executer les commandes suivantes:
```
composer install
yarn install
```

## Initialiser la base de données
```
php bin/console doctrine:database:create
php bin/console doctrine:migrations:migrate
```

Pour l'environnement de 'test'
```
php bin/console doctrine:database:create --e test
php bin/console doctrine:schema:update --force
```

Import des données de développement
```
php bin/console doctrine:fixtures:load
```

## Lancer la compilation des assets
Compiler une seule fois les fichiers en environnement de développement:
```
yarn encore dev
```

Compilation automatique
```
yarn watch
```

Compilation pour la production
```
yarn encore production
```

## Lancer serveur en local
Il est nécessaire d'avoir installé le [binaire de symfony](https://symfony.com/download).

```
symfony serve
```