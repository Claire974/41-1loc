# Documentation

## Sommaire

[1. Installation](1_installation.md)
[2. Environnement](2_environnement.md)
[3. Tests](3_tests.md)
[4. Uml](4_uml.md)