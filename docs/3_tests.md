# Tests

[Retour au sommaire](index.md)

Nous allons implementer différents types de test.

* **Test unitaire** : Tester unitairement un ensemble de classes
* **Test fonctionnel** : Tester l'interface en simulant une requête HTTP

## Pour lancer les test

```
php bin/phpunit --testdox
```

