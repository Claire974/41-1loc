<?php

namespace App\Tests;

use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase as TestKernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class KernelTestCase extends TestKernelTestCase
{
    protected ?object $validator;
    protected AbstractDatabaseTool $databaseTool;

    protected function setUp(): void
    {
        parent::setUp();
        self::bootKernel();
        $container = static::getContainer();
        $this->validator = $container->get(ValidatorInterface::class);
        $tool = $container->get(DatabaseToolCollection::class);
        if ($tool instanceof DatabaseToolCollection) {
            $this->databaseTool = $tool->get();
        } else {
            $this->fail('DatabaseToolCollection is unavailable');
        }
    }

    public function assertHasErrors(object $code, int $number = 0, ?string $group = null): void
    {
        if ($this->validator instanceof ValidatorInterface) {
            $errors = $this->validator->validate($code, null, $group);
            $messages = [];
            /** @var ConstraintViolation $error */
            foreach ($errors as $error) {
                $messages[] = $error->getPropertyPath().' => '.$error->getMessage();
            }
            $this->assertCount($number, $errors, implode(', ', $messages));
        } else {
            $this->fail('The validator is unavailable');
        }
    }
}
