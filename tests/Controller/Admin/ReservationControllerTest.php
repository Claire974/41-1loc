<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Reservation;
use App\Entity\User;
use App\Tests\WebTestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class ReservationControllerTest extends WebTestCase
{
    private const RESERVATION_URL = '/admin/reservation/';

    public function testAdminReservationIndex(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);
        $this->client->request('GET', self::RESERVATION_URL);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Liste des réservations');
    }

    public function testRedirectToLoginIfNotLogged(): void
    {
        $this->client->request('GET', self::RESERVATION_URL);
        $this->assertResponseRedirects('/connexion');
    }

    public function testNewReservationWithGoodValues(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::RESERVATION_URL.'ajouter');
        $form = $crawler->selectButton('Ajouter')->form([
            'reservation' => [
                'dateStart' => (new \DateTime())->modify('+20 days')->format('d-m-Y H:i'),
                'dateEnd' => (new \DateTime())->modify('+22 days')->format('d-m-Y H:i'),
                'user' => 1,
                'room' => 1,
            ],
        ]);
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');

        //clean invoice file
        $finder = new Finder();
        $filesystem = new Filesystem();
        $filesystem->remove($finder->in(__DIR__.'/../../../public/invoices')->name('test-*.pdf'));
    }

    public function testNewReservationWithDateBetweenExist(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::RESERVATION_URL.'ajouter');
        $form = $crawler->selectButton('Ajouter')->form([
            'reservation' => [
                'dateStart' => (new \DateTime())->modify('+3 days')->format('d-m-Y H:i'),
                'dateEnd' => (new \DateTime())->modify('+5 days')->format('d-m-Y H:i'),
                'user' => 1,
                'room' => 1,
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testNewReservationWithStartDateBeforeExist(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::RESERVATION_URL.'ajouter');
        $form = $crawler->selectButton('Ajouter')->form([
            'reservation' => [
                'dateStart' => (new \DateTime())->modify('+1 days')->format('d-m-Y H:i'),
                'dateEnd' => (new \DateTime())->modify('+5 days')->format('d-m-Y H:i'),
                'user' => 1,
                'room' => 1,
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testNewReservationWithDateFrameExist(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::RESERVATION_URL.'ajouter');
        $form = $crawler->selectButton('Ajouter')->form([
            'reservation' => [
                'dateStart' => (new \DateTime())->modify('+1 days')->format('d-m-Y H:i'),
                'dateEnd' => (new \DateTime())->modify('+15 days')->format('d-m-Y H:i'),
                'user' => 1,
                'room' => 1,
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testShowReservation(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Reservation $reservation */
        $reservation = $data['reservation1'];
        $this->client->request('GET', self::RESERVATION_URL.$reservation->getId());

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Réservation #'.$reservation->getId());
    }

    public function testCancelReservation(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Reservation $reservation */
        $reservation = $data['reservation1'];

        $crawler = $this->client->request('GET', self::RESERVATION_URL.$reservation->getId());
        $form = $crawler->selectButton('Annuler la réservation')->form();
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');
        $this->assertStringContainsString('Annulée', $this->client->getCrawler()->filter('table')->html());
    }

    public function testNewReservationWithEmptyValue(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::RESERVATION_URL.'ajouter');
        $form = $crawler->selectButton('Ajouter')->form([
            'reservation' => [],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(4);
    }

    public function testNewReservationWithStartBeforeNow(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::RESERVATION_URL.'ajouter');
        $form = $crawler->selectButton('Ajouter')->form([
            'reservation' => [
                'dateStart' => (new \DateTime())->modify('-2 days')->format('d-m-Y H:i'),
                'dateEnd' => (new \DateTime())->modify('+15 days')->format('d-m-Y H:i'),
                'user' => 1,
                'room' => 1,
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testNewReservationWithEndBeforeStart(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::RESERVATION_URL.'ajouter');
        $form = $crawler->selectButton('Ajouter')->form([
            'reservation' => [
                'dateStart' => (new \DateTime())->modify('+5 days')->format('d-m-Y H:i'),
                'dateEnd' => (new \DateTime())->modify('+2 days')->format('d-m-Y H:i'),
                'user' => 1,
                'room' => 1,
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testCancelReservationFinished(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Reservation $reservation */
        $reservation = $data['reservation_finished'];
        $this->client->request('GET', self::RESERVATION_URL.$reservation->getId());
        $this->assertStringNotContainsString('Annuler la réservation', $this->client->getCrawler()->html());
    }
}
