<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Category;
use App\Entity\User;
use App\Tests\WebTestCase;

class CategoryControllerTest extends WebTestCase
{
    private const CATEGORY_URL = '/admin/categorie/';

    public function testAdminCategoryIndex(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);
        $this->client->request('GET', self::CATEGORY_URL);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Liste des catégories');
    }

    public function testRedirectToLoginIfNotLogged(): void
    {
        $this->client->request('GET', self::CATEGORY_URL);
        $this->assertResponseRedirects('/connexion');
    }

    public function testNewCategoryWithGoodValues(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);
        $crawler = $this->client->request('GET', self::CATEGORY_URL.'ajouter');
        $form = $crawler->selectButton('Ajouter')->form([
            'category' => [
                'name' => 'Good Value',
            ],
        ]);
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testNewCategoryWithBadValues(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);
        $crawler = $this->client->request('GET', self::CATEGORY_URL.'ajouter');
        $form = $crawler->selectButton('Ajouter')->form([
            'category' => [
                'name' => '',
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testEditCategoryWithGoodValues(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/category.yaml',
            __DIR__.'/../../fixtures/user.yaml',
        ]);

        /** @var Category $category */
        $category = $data['category1'];

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::CATEGORY_URL.$category->getId().'/modifier');
        $form = $crawler->selectButton('Modifier')->form([
            'category' => [
                'name' => 'Good Edit Value',
            ],
        ]);
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testEditCateogryWithBadValues(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/category.yaml',
            __DIR__.'/../../fixtures/user.yaml',
        ]);

        /** @var Category $category */
        $category = $data['category1'];

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::CATEGORY_URL.$category->getId().'/modifier');
        $form = $crawler->selectButton('Modifier')->form([
            'category' => [
                'name' => '',
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testDeleteCategory(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/category.yaml',
            __DIR__.'/../../fixtures/user.yaml',
        ]);

        /** @var Category $category */
        $category = $data['category1'];

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::CATEGORY_URL.$category->getId().'/modifier');
        $form = $crawler->selectButton('Supprimer')->form();
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');
        $this->assertStringNotContainsString($category->getSlug(), $this->client->getCrawler()->filter('table')->html());
    }
}
