<?php

namespace App\Tests\Controller\Admin;

use App\Entity\User;
use App\Tests\WebTestCase;

class AdminControllerTest extends WebTestCase
{
    public function testAdminIndex(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);
        $this->client->request('GET', '/admin');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Bienvenue sur l\'espace d\'administration');
    }

    public function testRedirectToLoginIfNotLogged(): void
    {
        $this->client->request('GET', '/admin');
        $this->assertResponseRedirects('/connexion');
    }

    public function testAdminStat(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);
        $this->client->request('GET', '/admin/statistique');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Statistiques');
    }
}
