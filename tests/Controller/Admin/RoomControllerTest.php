<?php

namespace App\Tests\Controller\Admin;

use App\Entity\Room;
use App\Entity\User;
use App\Tests\WebTestCase;

class RoomControllerTest extends WebTestCase
{
    private const ROOM_URL = '/admin/salle/';

    public function testAdminRoomIndex(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);
        $this->client->request('GET', self::ROOM_URL);

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Liste des salles');
    }

    public function testRedirectToLoginIfNotLogged(): void
    {
        $this->client->request('GET', self::ROOM_URL);
        $this->assertResponseRedirects('/connexion');
    }

    public function testNewRoomWithGoodValues(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/category.yaml',
            __DIR__.'/../../fixtures/user.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::ROOM_URL.'ajouter');
        $form = $crawler->selectButton('Ajouter')->form([
            'room' => [
                'name' => 'Good Value',
                'isActive' => false,
                'surface' => 10,
                'seat' => 5,
                'price' => 40.2,
                'description' => 'Good value',
                'category' => 1,
            ],
        ]);
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testNewRoomWithBadValues(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        $crawler = $this->client->request('GET', self::ROOM_URL.'ajouter');
        $form = $crawler->selectButton('Ajouter')->form([
            'room' => [
                'name' => '',
                'isActive' => false,
                'surface' => -10,
                'seat' => -5,
                'price' => -40.2,
                'description' => '',
                'category' => '',
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(6);
    }

    public function testShowRoom(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/room.yaml',
            __DIR__.'/../../fixtures/user.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Room $room */
        $room = $data['room1'];
        $this->client->request('GET', self::ROOM_URL.$room->getId());

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $room->getName());
    }

    public function testEditRoomWithGoodValues(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/room.yaml',
            __DIR__.'/../../fixtures/user.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Room $room */
        $room = $data['room1'];

        $crawler = $this->client->request('GET', self::ROOM_URL.$room->getId().'/modifier');
        $form = $crawler->selectButton('Modifier')->form([
            'room' => [
                'name' => 'Good Value',
                'isActive' => false,
                'surface' => 10,
                'seat' => 5,
                'price' => 40.2,
                'description' => 'Good value',
                'category' => 3,
            ],
        ]);
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');
    }

    public function testEditRoomWithBadValues(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/room.yaml',
            __DIR__.'/../../fixtures/user.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Room $room */
        $room = $data['room1'];

        $crawler = $this->client->request('GET', self::ROOM_URL.$room->getId().'/modifier');
        $form = $crawler->selectButton('Modifier')->form([
            'room' => [
                'name' => '',
                'isActive' => false,
                'surface' => -10,
                'seat' => -5,
                'price' => -40.2,
                'description' => '',
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(5);
    }

    public function testDeleteRoom(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/room.yaml',
            __DIR__.'/../../fixtures/user.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Room $room */
        $room = $data['room1'];

        $crawler = $this->client->request('GET', self::ROOM_URL.$room->getId().'/modifier');
        $form = $crawler->selectButton('Supprimer')->form();
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');
        // TODO: Changer le getName pour getSlug
        $this->assertStringNotContainsString($room->getName(), $this->client->getCrawler()->filter('table')->html());
    }

    public function testToggleIsActive(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/room.yaml',
            __DIR__.'/../../fixtures/user.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Room $room */
        $room = $data['room1'];

        $status = $room->getIsActive();
        $invert = !$status;
        $this->client->request('PATCH', self::ROOM_URL.'switch/'.$room->getId());
        $this->assertResponseIsSuccessful();
        $response = json_decode((string) $this->client->getResponse()->getContent(), true);
        $this->assertTrue($response['value'] === $invert);
    }
}
