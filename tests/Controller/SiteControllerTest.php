<?php

namespace App\Tests\Controller;

use App\Entity\Category;
use App\Entity\Room;
use App\Entity\User;
use App\Tests\WebTestCase;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Response;

class SiteControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $this->client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Réservez votre bureau à Meylan en toute simplicité');
    }

    public function testAbout(): void
    {
        $this->client->request('GET', '/fonctionnement');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Fonctionnement du site');
    }

    public function testLegals(): void
    {
        $this->client->request('GET', '/mentions-legales');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Mentions légales');
    }

    public function testPageContact(): void
    {
        $this->client->request('GET', '/contact');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Nous contacter');
    }

    public function testContactFormWithGoodValues(): void
    {
        $crawler = $this->client->request('GET', '/contact');

        $form = $crawler->selectButton('Envoyer')->form([
            'contact' => [
                'name' => 'Paul Auchon',
                'email' => 'paul@auchon.local',
                'subject' => 1,
                'message' => 'Good message',
                'gdpr' => true,
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(0);
        $this->assertEmailCount(1);
    }

    public function testContactFormWithEmptyValues(): void
    {
        $crawler = $this->client->request('GET', '/contact');

        $form = $crawler->selectButton('Envoyer')->form([
            'contact' => [
                'name' => '',
                'email' => '',
                'subject' => '',
                'message' => '',
                'gdpr' => false,
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(5);
        $this->assertEmailCount(0);
    }

    public function testContactFormWithBadEmail(): void
    {
        $crawler = $this->client->request('GET', '/contact');

        $form = $crawler->selectButton('Envoyer')->form([
            'contact' => [
                'name' => 'Paul Auchon',
                'email' => 'bad',
                'subject' => 1,
                'message' => 'Good message',
                'gdpr' => true,
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
        $this->assertEmailCount(0);
    }

    public function testContactReservationFormWithGoodValues(): void
    {
        $crawler = $this->client->request('GET', '/contact/reservation');
        $form = $crawler->selectButton('Envoyer')->form([
            'contact' => [
                'name' => 'Paul Auchon',
                'email' => 'paul@auchon.local',
                'subject' => 2,
                'date' => '2021-08-04',
                'message' => 'Good message',
                'gdpr' => true,
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(0);
        $this->assertEmailCount(1);
    }

    public function testContactReservationFormWithEmptyDate(): void
    {
        $crawler = $this->client->request('GET', '/contact/reservation');
        $form = $crawler->selectButton('Envoyer')->form([
            'contact' => [
                'name' => 'Paul Auchon',
                'email' => 'paul@auchon.local',
                'subject' => 2,
                'date' => '',
                'message' => 'Good message',
                'gdpr' => true,
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
        $this->assertEmailCount(0);
    }

    public function testShowAllRooms(): void
    {
        $this->client->request('GET', '/nos-salles');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Nos salles');
    }

    public function testShowRoom(): void
    {
        /** @var Room[] $rooms */
        $rooms = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/room.yaml',
        ]);
        $room1 = $rooms['room1'];
        $this->client->request('GET', '/nos-salles/'.$room1->getSlug());
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $room1->getName());
        $this->assertSelectorTextContains('.alert-danger', 'Vous devez être connecté pour réserver');
    }

    public function testShowRoomWithBadSlug(): void
    {
        /** @var Room[] $rooms */
        $rooms = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/room.yaml',
        ]);

        $this->client->request('GET', '/nos-salles/vhfujeldszhbvl');
        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function testShowCategory(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/room.yaml',
            __DIR__.'/../fixtures/category.yaml',
        ]);
        /** @var Category $cat1 */
        $cat1 = $data['category1'];
        $this->client->request('GET', '/nos-salles/categorie/'.$cat1->getSlug());
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $cat1->getName());
    }

    public function testShowCategoryWithBadSlug(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/room.yaml',
            __DIR__.'/../fixtures/category.yaml',
        ]);

        $this->client->request('GET', '/nos-salles/categorie/vhfujeldszhbvl');
        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function testShowRoomLogged(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/room.yaml',
            __DIR__.'/../fixtures/user.yaml',
        ]);
        /** @var Room $room1 */
        $room1 = $data['room1'];

        /** @var User $user1 */
        $user1 = $data['user1'];

        $this->client->loginUser($user1);
        $this->client->request('GET', '/nos-salles/'.$room1->getSlug());
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', $room1->getName());
        $this->assertSelectorTextContains('.btn-primary', 'Réserver');
    }

    public function testReservationNotLogged(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/room.yaml',
        ]);

        /** @var Room $room1 */
        $room1 = $data['room1'];

        $this->client->request('GET', '/nos-salles/'.$room1->getSlug().'/reserver');
        $this->assertResponseRedirects('/connexion');
    }

    public function testReservationWithInvalidSlug(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/room.yaml',
            __DIR__.'/../fixtures/user.yaml',
        ]);

        /** @var User $user1 */
        $user1 = $data['user1'];

        $this->client->loginUser($user1);
        $this->client->request('GET', '/nos-salles/hfvuijrelshng/reserver');
        $this->assertResponseStatusCodeSame(Response::HTTP_NOT_FOUND);
    }

    public function testNewReservationWithGoodValues(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Room $room1 */
        $room1 = $data['room1'];

        $crawler = $this->client->request('GET', '/nos-salles/'.$room1->getSlug().'/reserver');
        $form = $crawler->selectButton('Réserver')->form([
            'reservation' => [
                'dateStart' => (new \DateTime())->modify('+20 days')->format('d-m-Y H:i'),
                'dateEnd' => (new \DateTime())->modify('+22 days')->format('d-m-Y H:i'),
            ],
        ]);
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-success');

        //clean invoice file
        $finder = new Finder();
        $filesystem = new Filesystem();
        $filesystem->remove($finder->in(__DIR__.'/../../public/invoices')->name('test-*.pdf'));
    }

    public function testNewReservationWithDateBetweenExist(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Room $room1 */
        $room1 = $data['room1'];

        $crawler = $this->client->request('GET', '/nos-salles/'.$room1->getSlug().'/reserver');
        $form = $crawler->selectButton('Réserver')->form([
            'reservation' => [
                'dateStart' => (new \DateTime())->modify('+3 days')->format('d-m-Y H:i'),
                'dateEnd' => (new \DateTime())->modify('+5 days')->format('d-m-Y H:i'),
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testNewReservationWithStartDateBeforeExist(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Room $room1 */
        $room1 = $data['room1'];

        $crawler = $this->client->request('GET', '/nos-salles/'.$room1->getSlug().'/reserver');
        $form = $crawler->selectButton('Réserver')->form([
            'reservation' => [
                'dateStart' => (new \DateTime())->modify('+1 days')->format('d-m-Y H:i'),
                'dateEnd' => (new \DateTime())->modify('+5 days')->format('d-m-Y H:i'),
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testNewReservationWithDateFrameExist(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Room $room1 */
        $room1 = $data['room1'];

        $crawler = $this->client->request('GET', '/nos-salles/'.$room1->getSlug().'/reserver');
        $form = $crawler->selectButton('Réserver')->form([
            'reservation' => [
                'dateStart' => (new \DateTime())->modify('+1 days')->format('d-m-Y H:i'),
                'dateEnd' => (new \DateTime())->modify('+15 days')->format('d-m-Y H:i'),
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testNewReservationWithEmptyValue(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Room $room1 */
        $room1 = $data['room1'];

        $crawler = $this->client->request('GET', '/nos-salles/'.$room1->getSlug().'/reserver');
        $form = $crawler->selectButton('Réserver')->form([
            'reservation' => [],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(2);
    }

    public function testNewReservationWithStartBeforeNow(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Room $room1 */
        $room1 = $data['room1'];

        $crawler = $this->client->request('GET', '/nos-salles/'.$room1->getSlug().'/reserver');
        $form = $crawler->selectButton('Réserver')->form([
            'reservation' => [
                'dateStart' => (new \DateTime())->modify('-2 days')->format('d-m-Y H:i'),
                'dateEnd' => (new \DateTime())->modify('+15 days')->format('d-m-Y H:i'),
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testNewReservationWithEndBeforeStart(): void
    {
        $data = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/reservation.yaml',
        ]);

        /** @var User $admin */
        $admin = $data['admin'];
        $this->client->loginUser($admin);

        /** @var Room $room1 */
        $room1 = $data['room1'];

        $crawler = $this->client->request('GET', '/nos-salles/'.$room1->getSlug().'/reserver');
        $form = $crawler->selectButton('Réserver')->form([
            'reservation' => [
                'dateStart' => (new \DateTime())->modify('+5 days')->format('d-m-Y H:i'),
                'dateEnd' => (new \DateTime())->modify('+2 days')->format('d-m-Y H:i'),
            ],
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }
}
