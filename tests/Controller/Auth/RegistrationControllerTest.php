<?php

namespace App\Tests\Controller\Auth;

use App\Entity\User;
use App\Tests\WebTestCase;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class RegistrationControllerTest extends WebTestCase
{
    private const REGISTER_URL = '/inscription';
    private const REGISTER_FORM_SUBMIT = 'S\'inscrire';
    private const REGISTER_CHECK_EMAIL = '/confirmation-email';

    public function testRegisterPage(): void
    {
        $this->client->request('GET', self::REGISTER_URL);
        $this->assertResponseIsSuccessful();
    }

    public function testRegisterWithGoodValue(): void
    {
        $crawler = $this->client->request('GET', self::REGISTER_URL);

        $form = $crawler->selectButton(self::REGISTER_FORM_SUBMIT)->form(
            ['registration_form' => [
                'firstName' => 'alain',
                'lastName' => 'Terrieur',
                'phone' => '0612345678',
                'address' => '33 rue bidon',
                'zipCode' => '38100',
                'city' => 'Grenoble',
                'email' => 'alain@terieur.local',
                'password' => [
                    'first' => 'hH2m3T7An',
                    'second' => 'hH2m3T7An',
                ],
                'agreeTerms' => true,
            ]]
        );
        $this->client->submit($form);
        $this->expectFormErrors(0);
        $this->assertEmailCount(1);
        $this->assertResponseRedirects('/connexion');

        $email = $this->getMailerMessage();
        if ($email instanceof TemplatedEmail) {
            $context = $email->getContext();
            $url = $context['signedUrl'];
            $this->client->request('GET', $url);
            $this->assertResponseRedirects('/connexion');
        } else {
            $this->assertEquals(1, 0, 'Impossible de lire le mail');
        }
    }

    public function testRegisterWithEmptyValues(): void
    {
        $crawler = $this->client->request('GET', self::REGISTER_URL);

        $form = $crawler->selectButton(self::REGISTER_FORM_SUBMIT)->form(
            ['registration_form' => [
                'firstName' => '',
                'lastName' => '',
                'phone' => '',
                'address' => '',
                'zipCode' => '',
                'city' => '',
                'email' => '',
                'password' => [
                    'first' => '',
                    'second' => '',
                ],
                'agreeTerms' => false,
            ]]
        );
        $this->client->submit($form);
        $this->expectFormErrors(9);
        $this->assertEmailCount(0);
    }

    public function testRegisterWithBadEmail(): void
    {
        $crawler = $this->client->request('GET', self::REGISTER_URL);

        $form = $crawler->selectButton(self::REGISTER_FORM_SUBMIT)->form(
            ['registration_form' => [
                'firstName' => 'alain',
                'lastName' => 'Terrieur',
                'phone' => '0612345678',
                'address' => '33 rue bidon',
                'zipCode' => '38100',
                'city' => 'Grenoble',
                'email' => 'badEmail',
                'password' => [
                    'first' => 'hH2m3T7An',
                    'second' => 'hH2m3T7An',
                ],
                'agreeTerms' => true,
            ]]
        );
        $this->client->submit($form);
        $this->expectFormErrors(1);
        $this->assertEmailCount(0);
    }

    public function testRegisterWithBadPassword(): void
    {
        $crawler = $this->client->request('GET', self::REGISTER_URL);

        $form = $crawler->selectButton(self::REGISTER_FORM_SUBMIT)->form(
            ['registration_form' => [
                'firstName' => 'alain',
                'lastName' => 'Terrieur',
                'phone' => '0612345678',
                'address' => '33 rue bidon',
                'zipCode' => '38100',
                'city' => 'Grenoble',
                'email' => 'bad@password.local',
                'password' => [
                    'first' => 'BadPassword',
                    'second' => 'BadPassword',
                ],
                'agreeTerms' => true,
            ]]
        );
        $this->client->submit($form);
        $this->expectFormErrors(1);
        $this->assertEmailCount(0);
    }

    public function testRegisterWithMismatchPassword(): void
    {
        $crawler = $this->client->request('GET', self::REGISTER_URL);

        $form = $crawler->selectButton(self::REGISTER_FORM_SUBMIT)->form(
            ['registration_form' => [
                'firstName' => 'alain',
                'lastName' => 'Terrieur',
                'phone' => '0612345678',
                'address' => '33 rue bidon',
                'zipCode' => '38100',
                'city' => 'Grenoble',
                'email' => 'bad@password.local',
                'password' => [
                    'first' => 'hH2m3T7An',
                    'second' => 'ZH2m3T7An',
                ],
                'agreeTerms' => true,
            ]]
        );
        $this->client->submit($form);
        $this->expectFormErrors(1);
        $this->assertEmailCount(0);
    }

    public function testRegisterWithExistingInfo(): void
    {
        $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);

        $crawler = $this->client->request('GET', self::REGISTER_URL);

        $form = $crawler->selectButton(self::REGISTER_FORM_SUBMIT)->form(
            ['registration_form' => [
                'firstName' => 'alain',
                'lastName' => 'Terrieur',
                'phone' => '0612345678',
                'address' => '33 rue bidon',
                'zipCode' => '38100',
                'city' => 'Grenoble',
                'email' => 'paul@auchon.local',
                'password' => [
                    'first' => 'hH2m3T7An',
                    'second' => 'hH2m3T7An',
                ],
                'agreeTerms' => true,
            ]]
        );
        $this->client->submit($form);
        $this->expectFormErrors(1);
        $this->assertEmailCount(0);
    }

    public function testRedirectIfAdminLogged(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $user = $users['admin'];
        $this->client->loginUser($user);
        $this->client->request('GET', self::REGISTER_URL);
        $this->assertResponseRedirects('/admin');
    }

    public function testRedirectIfUserLogged(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $user = $users['user1'];
        $this->client->loginUser($user);
        $this->client->request('GET', self::REGISTER_URL);
        $this->assertResponseRedirects('/mon-compte');
    }

    public function testVerifyUserEmailWithoutId(): void
    {
        $this->client->request('GET', self::REGISTER_CHECK_EMAIL);
        $this->assertResponseRedirects(self::REGISTER_URL);
    }

    public function testVerifyUserWithUnknownId(): void
    {
        $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $this->client->request('GET', self::REGISTER_CHECK_EMAIL.'?id=1000');
        $this->assertResponseRedirects(self::REGISTER_URL);
    }

    public function testVerifyUserEmailWithInvalidUrl(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);

        $user = $users['unverified'];

        $this->client->request('GET', self::REGISTER_CHECK_EMAIL.'?id='.$user->getId());
        $this->assertResponseRedirects(self::REGISTER_URL);
    }
}
