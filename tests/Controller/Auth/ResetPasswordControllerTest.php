<?php

namespace App\Tests\Controller\Auth;

use App\Entity\User;
use App\Tests\WebTestCase;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class ResetPasswordControllerTest extends WebTestCase
{
    private const RESET_REQUEST_URL = '/mot-de-passe-perdu';
    private const RESET_PASSWORD_URL = self::RESET_REQUEST_URL.'/reinitialiser';

    public function testResetPasswordFromLogin(): void
    {
        $this->client->request('GET', '/connexion');
        $this->client->click($this->client->getCrawler()->selectLink('Mot de passe perdu')->link());
        $this->assertSelectorTextContains('h1', 'Reinitialiser mon mot de passe');
    }

    public function testRedirectIfAlreadyLoggedIn(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];
        $this->client->loginUser($admin);
        $this->client->request('GET', self::RESET_REQUEST_URL);

        $this->assertResponseRedirects('/mon-compte');
    }

    public function testResetPasswordWithBadValue(): void
    {
        $crawler = $this->client->request('GET', self::RESET_REQUEST_URL);
        $form = $crawler->selectButton('Envoyer')->form([
            'reset_password_request_form[email]' => 'badEmail',
        ]);
        $this->client->submit($form);
        $this->expectFormErrors(1);
    }

    public function testResetPasswordWithUnknownUser(): void
    {
        $crawler = $this->client->request('GET', self::RESET_REQUEST_URL);
        $form = $crawler->selectButton('Envoyer')->form([
            'reset_password_request_form[email]' => 'unknown@user.local',
        ]);
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertEmailCount(0);
    }

    public function testResetPasswordWithKnownUser(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $user = $users['paul'];

        $crawler = $this->client->request('GET', self::RESET_REQUEST_URL);
        $form = $crawler->selectButton('Envoyer')->form([
            'reset_password_request_form[email]' => $user->getEmail(),
        ]);
        $this->client->submit($form);
        $this->client->followRedirects();
        $this->assertEmailCount(1);
        $email = $this->getMailerMessage();
        if ($email instanceof TemplatedEmail) {
            $body = (string) $email->getHtmlBody();
            $links = $this->extractLinks($body);

            if (count($links) > 0) {
                $this->client->request('GET', $links[0]);
                $this->client->followRedirects();

                $crawler = $this->client->getCrawler();
                $form = $crawler->selectButton('Reinitialiser')->form([
                    'change_password_form' => [
                        'password' => [
                            'first' => 'hH2m3T7An',
                            'second' => 'hH2m3T7An',
                        ],
                    ],
                ]);

                $this->client->submit($form);
                $this->assertSelectorExists('.alert-success');
            } else {
                $this->assertEquals(1, 0, 'Impossible de lire le mail');
            }
        } else {
            $this->assertEquals(1, 0, 'Impossible de lire le mail');
        }
    }

    public function testResetPasswordAlreadySend(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $user = $users['paul'];

        $crawler = $this->client->request('GET', self::RESET_REQUEST_URL);
        $form = $crawler->selectButton('Envoyer')->form([
            'reset_password_request_form[email]' => $user->getEmail(),
        ]);
        $this->client->submit($form);
        $this->client->followRedirects();

        $crawler = $this->client->request('GET', self::RESET_REQUEST_URL);
        $form = $crawler->selectButton('Envoyer')->form();
        $form->setValues([
            'reset_password_request_form[email]' => $user->getEmail(),
        ]);
        $this->client->submit($form);
        $this->assertSelectorExists('.alert-danger');
    }

    public function testResetPasswordWithoutToken(): void
    {
        $this->client->request('GET', self::RESET_PASSWORD_URL);
        $this->assertResponseRedirects(self::RESET_REQUEST_URL);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-danger');
    }

    public function testResetPasswordWithInvalidToken(): void
    {
        $this->client->request('GET', self::RESET_PASSWORD_URL.'/azeaze');
        $this->client->followRedirect();
        $this->assertResponseRedirects(self::RESET_REQUEST_URL);
    }

    private function extractLinks(string $text): array
    {
        $linkArray = [];
        if (preg_match_all('/<a\s+.*?href=[\"\']?([^\"\' >]*)[\"\']?[^>]*>(.*?)<\/a>/i', $text, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $match) {
                array_push($linkArray, $match[2]);
            }
        }

        return $linkArray;
    }
}
