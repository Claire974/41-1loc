<?php

namespace App\Tests\Controller\Auth;

use App\Entity\User;
use App\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class SecurityControllerTest extends WebTestCase
{
    private const LOGIN_URL = '/connexion';
    private const LOGIN_FORM_SUBMIT = 'Connexion';

    // Todo: Gérer la redirection en fonction du rôle

    public function testLoginPage(): void
    {
        $this->client->request('GET', self::LOGIN_URL);
        $this->assertResponseIsSuccessful();
    }

    public function testLoginWithEmptyValues(): void
    {
        $crawler = $this->client->request('GET', self::LOGIN_URL);

        $form = $crawler->selectButton(self::LOGIN_FORM_SUBMIT)->form([
            'email' => '',
            'password' => '',
        ]);
        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-danger');
    }

    public function testLoginWithBadCombinaison(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $paul = $users['paul'];

        $crawler = $this->client->request('GET', self::LOGIN_URL);

        $form = $crawler->selectButton(self::LOGIN_FORM_SUBMIT)->form([
            'email' => $paul->getEmail(),
            'password' => 'WrongPassword',
        ]);

        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-danger');
    }

    public function testLoginWithBadCsrfToken(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $paul = $users['paul'];

        $crawler = $this->client->request('GET', self::LOGIN_URL);

        $form = $crawler->selectButton(self::LOGIN_FORM_SUBMIT)->form([
            'email' => $paul->getEmail(),
            'password' => 'hH2m3T7An',
            '_csrf_token' => 'bad',
        ]);

        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-danger');
    }

    public function testLoginWithSimpleAccount(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $paul = $users['paul'];

        $crawler = $this->client->request('GET', self::LOGIN_URL);

        $form = $crawler->selectButton(self::LOGIN_FORM_SUBMIT)->form([
            'email' => $paul->getEmail(),
            'password' => 'hH2m3T7An',
        ]);

        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertRouteSame('profile_show');
        $this->assertResponseIsSuccessful();
    }

    public function testLoginWithAdminAccount(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $admin = $users['admin'];

        $crawler = $this->client->request('GET', self::LOGIN_URL);

        $form = $crawler->selectButton(self::LOGIN_FORM_SUBMIT)->form([
            'email' => $admin->getEmail(),
            'password' => 'hH2m3T7An',
        ]);

        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertRouteSame('admin_index');
        $this->assertResponseIsSuccessful();
    }

    public function testLoginWithBannedAccount(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $banned = $users['banned'];

        $crawler = $this->client->request('GET', self::LOGIN_URL);

        $form = $crawler->selectButton(self::LOGIN_FORM_SUBMIT)->form([
            'email' => $banned->getEmail(),
            'password' => 'hH2m3T7An',
        ]);

        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-danger');
    }

    public function testRedirectIfAdminLogged(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $user = $users['admin'];
        $this->client->loginUser($user);
        $this->client->request('GET', self::LOGIN_URL);
        $this->assertResponseRedirects('/admin');
    }

    public function testRedirectIfUserLogged(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $user = $users['user1'];
        $this->client->loginUser($user);
        $this->client->request('GET', self::LOGIN_URL);
        $this->assertResponseRedirects('/mon-compte');
    }

    public function testLogout(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $user = $users['admin'];
        $this->client->loginUser($user);
        $this->client->request('GET', '/deconnexion');
        $this->assertResponseRedirects('', Response::HTTP_FOUND);
        /** @var Session $session */
        $session = $this->getContainer()->get('session');
        $this->assertNull($session->get('_security_main'));
    }

    public function testLoginWithUnverifiedAccount(): void
    {
        /** @var User[] $users */
        $users = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../../fixtures/user.yaml',
        ]);
        $unverified = $users['unverified'];

        $crawler = $this->client->request('GET', self::LOGIN_URL);

        $form = $crawler->selectButton(self::LOGIN_FORM_SUBMIT)->form([
            'email' => $unverified->getEmail(),
            'password' => 'hH2m3T7An',
        ]);

        $this->client->submit($form);
        $this->client->followRedirect();
        $this->assertSelectorExists('.alert-danger');
    }
}
