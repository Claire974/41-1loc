<?php

namespace App\Tests\Controller;

use App\Tests\WebTestCase;

class ApiControllerTest extends WebTestCase
{
    public function testApiIndex(): void
    {
        $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/reservation.yaml',
        ]);

        $this->client->request('GET', '/api');

        $this->assertResponseIsSuccessful();
        $this->assertJson((string) $this->client->getResponse()->getContent());
    }
}
