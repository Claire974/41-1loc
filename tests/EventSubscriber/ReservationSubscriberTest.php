<?php

namespace App\Test\EventSubscriber;

use App\Event\CreateReservationEvent;
use App\EventSubscriber\ReservationSubscriber;
use PHPUnit\Framework\TestCase;

class ReservationSubscriberTest extends TestCase
{
    public function testEventSubscription(): void
    {
        $this->assertArrayHasKey(CreateReservationEvent::class, ReservationSubscriber::getSubscribedEvents());
    }
}
