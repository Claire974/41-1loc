<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Room;
use App\Tests\KernelTestCase;

class RoomEntityTest extends KernelTestCase
{
    private function getEntity(): Room
    {
        $category = (new Category())->setName('Good value');

        return (new Room())
            ->setName('Good Value')
            ->setIsActive(true)
            ->setPrice(10.6)
            ->setSeat(4)
            ->setSurface(9.72)
            ->setDescription('Good Value')
            ->setCategory($category)
        ;
    }

    public function testWithGoodValues(): void
    {
        $this->assertHasErrors($this->getEntity());
    }

    public function testWithEmptyValues(): void
    {
        $this->assertHasErrors($this->getEntity()->setName('')->setDescription(''), 2);
    }

    public function testWithNegativeValues(): void
    {
        $this->assertHasErrors($this->getEntity()->setPrice(-10)->setSurface(-10)->setSeat(-10), 3);
    }
}
