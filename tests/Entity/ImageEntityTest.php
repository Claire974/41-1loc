<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Entity\Image;
use App\Entity\Room;
use App\Tests\KernelTestCase;
use DateTime;
use Symfony\Component\HttpFoundation\File\File;

class ImageEntityTest extends KernelTestCase
{
    private function getEntity(): Image
    {
        $category = (new Category())->setName('Good value');

        $room = (new Room())
            ->setName('Good Value')
            ->setIsActive(true)
            ->setPrice(10.6)
            ->setSeat(4)
            ->setSurface(9.72)
            ->setDescription('Good Value')
            ->setCategory($category)
        ;

        return (new Image())
            ->setName('Good Value')
            ->setImageFile(new File(__DIR__.'/../fixtures/fixture.png'))
            ->setRoom($room)
            ->setUpdatedAt(new DateTime())
            ->setPath('path')
        ;
    }

    public function testWithGoodValues(): void
    {
        $entity = $this->getEntity();
        $this->assertHasErrors($entity);
        $this->assertTrue($entity->getUpdatedAt()->format('Y-m-d H:i') === (new DateTime())->format('Y-m-d H:i'));
        $room = $entity->getRoom();
        if ($room) {
            $this->assertTrue($room->getName() === 'Good Value');
        }
        $this->assertTrue($entity->getName() === 'Good Value');
        $this->assertTrue($entity->getPath() === 'path');
    }

    public function testWithEmptyValues(): void
    {
        $this->assertHasErrors($this->getEntity()->setName(''), 1);
    }
}
