<?php

namespace App\Tests\Entity;

use App\Entity\Category;
use App\Tests\KernelTestCase;

class CategoryEntityTest extends KernelTestCase
{
    private function getEntity(): Category
    {
        return (new Category())
            ->setName('Good Value')
            ->setSlug('good-slug')
        ;
    }

    public function testWithGoodValues(): void
    {
        $this->assertHasErrors($this->getEntity());
    }

    public function testWithEmptyValues(): void
    {
        $this->assertHasErrors($this->getEntity()->setName(''), 1);
    }

    public function testAlreadyExist(): void
    {
        /** @var Category[] $categories */
        $categories = $this->databaseTool->loadAliceFixture([
            __DIR__.'/../fixtures/category.yaml',
        ]);

        $this->assertHasErrors($this->getEntity()->setName($categories['category1']->getName()), 1);
    }
}
