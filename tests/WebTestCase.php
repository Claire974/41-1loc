<?php

namespace App\Tests;

use Liip\TestFixturesBundle\Services\DatabaseToolCollection;
use Liip\TestFixturesBundle\Services\DatabaseTools\AbstractDatabaseTool;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as TestWebTestCase;

class WebTestCase extends TestWebTestCase
{
    public const FORM_ERROR_CLASS = '.invalid-feedback';
    protected KernelBrowser $client;
    protected AbstractDatabaseTool $databaseTool;

    protected function setUp(): void
    {
        parent::setUp();
        $this->client = self::createClient();
        $container = static::getContainer();
        $tool = $container->get(DatabaseToolCollection::class);
        if ($tool instanceof DatabaseToolCollection) {
            $this->databaseTool = $tool->get();
        } else {
            $this->fail('DatabaseToolCollection is unavailable');
        }
    }

    /**
     * Check that the form contains the number of errors indicated.
     */
    public function expectFormErrors(int $expectedErrors, string $class = self::FORM_ERROR_CLASS): void
    {
        $this->assertEquals($expectedErrors, $this->client->getCrawler()->filter($class)->count(), 'Form errors missmatch');
    }
}
